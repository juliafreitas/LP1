#include <EEPROM.h>

int posicao = 0;

// prototipos de funcao
void imprimeByte(byte* endloc);
void escreveByte(int );

void setup() 
{
  // put your setup code here, to run once:
  Serial.begin(9600);
  
//  escreveByte(&EEPROM[posicao], 0);
  //EEPROM.update(10, 27);
}

void loop() 
{
  Serial.print("posicao: ");
  Serial.println(posicao);
  
  byte valor_atual = EEPROM[posicao]; // le o valor do indice da EEPROM

  imprimeByte(&valor_atual); // imprime os valores no serial monitor
  
  posicao = posicao + 1;

  if ( posicao == EEPROM.length() ) // chega ao final da memoria e volta pro inicio
    posicao = 0;
   
}

// recebe o end da variavel - imprime a variavel no monitor serial
void imprimeByte(byte* endloc)
{
  //byte valor = EEPROM.read(*endloc);
  Serial.print("valor do endereco ");
  Serial.print(posicao);
  Serial.print(": ");
  Serial.println(*endloc);
}  
/*
void escreveByte(int* endloc, char valor)
{
  EEPROM.update(*endloc, valor);
}
*/

