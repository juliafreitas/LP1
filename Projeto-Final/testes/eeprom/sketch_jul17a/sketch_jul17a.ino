#include <EEPROM.h>

// start reading from the first byte (address 0) of the EEPROM
int address = 0;
byte value;

void setup()
{
  Serial.begin(9600);
}

void loop()
{
  // lê um byte do endereço atual da EEPROM
  value = EEPROM.read(address);

  Serial.print(address);
  Serial.print("\t");
  Serial.print(value, DEC);
  Serial.println();

  // avance ao próximo endereço da EEPROM
  address = address + 1;

  // há apenas 512 bytes na EEPROM, de 0 a 511, de forma que se
  // estivermos no endereço 512, meia-volta ao 0
  if (address == 512)
    address = 0;

  delay(500);
}


